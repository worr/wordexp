#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <wordexp.h>

void
single_word(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("foo", &pwordexp, 0);

	g_assert(pwordexp.we_wordc == 1);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "foo");
	g_assert(pwordexp.we_wordv[1] == NULL);
	g_assert(ret == 0);

	wordfree(&pwordexp);
}

void
multiple_words(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("foo bar", &pwordexp, 0);

	g_assert(pwordexp.we_wordc == 2);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "foo");
	g_assert_cmpstr(pwordexp.we_wordv[1], ==, "bar");
	g_assert(pwordexp.we_wordv[2] == NULL);
	g_assert(ret == 0);

	wordfree(&pwordexp);
}

void
no_words(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("", &pwordexp, 0);

	g_assert(pwordexp.we_wordc == 0);
	g_assert(pwordexp.we_wordv[0] == NULL);
	g_assert(ret == 0);

	wordfree(&pwordexp);
}

void
simple_append(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("foo", &pwordexp, 0);

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 1);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "foo");

	ret = wordexp("bar", &pwordexp, WRDE_APPEND);

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 2);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "foo");
	g_assert_cmpstr(pwordexp.we_wordv[1], ==, "bar");

	wordfree(&pwordexp);
}

void
simple_ooffs(void)
{
	int ret = 0;
	wordexp_t pwordexp = {
		.we_offs = 1,
	};
	ret = wordexp("foo", &pwordexp, WRDE_DOOFFS);

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 1);
	g_assert(pwordexp.we_wordv[0] == NULL);
	g_assert_cmpstr(pwordexp.we_wordv[1], ==, "foo");
	g_assert(pwordexp.we_wordv[2] == NULL);

	wordfree(&pwordexp);
}

void
simple_reuse(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("foo", &pwordexp, 0);

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 1);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "foo");
	g_assert(pwordexp.we_wordv[1] == NULL);

	ret = wordexp("bar", &pwordexp, WRDE_REUSE);

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 1);
	g_assert_cmpstr(pwordexp.we_wordv[0], ==, "bar");
	g_assert(pwordexp.we_wordv[1] == NULL);

	wordfree(&pwordexp);

}

void
comments(void)
{
	int ret = 0;
	wordexp_t pwordexp;
	memset(&pwordexp, 0, sizeof(pwordexp));
	ret = wordexp("# this is a comment", &pwordexp, 0);

	g_printerr("Count: %zu\n", pwordexp.we_wordc);
	for (size_t i = 0; i < pwordexp.we_wordc; i++)
		g_printerr("%s ", pwordexp.we_wordv[i]);
	g_printerr("\n");

	g_assert(ret == 0);
	g_assert(pwordexp.we_wordc == 0);
	g_assert(pwordexp.we_wordv[0] == NULL);

	wordfree(&pwordexp);
}

int
main(int argc, char **argv)
{
	g_test_init(&argc, &argv, NULL);

	g_test_add_func("/wordexp/01_basic/single_word", single_word);
	g_test_add_func("/wordexp/01_basic/multiple_words", multiple_words);
	g_test_add_func("/wordexp/01_basic/no_words", no_words);
	g_test_add_func("/wordexp/01_basic/simple_append", simple_append);
	g_test_add_func("/wordexp/01_basic/simple_ooffs", simple_ooffs);
	g_test_add_func("/wordexp/01_basic/simple_reuse", simple_reuse);
	g_test_add_func("/wordexp/01_basic/comments", comments);

	return g_test_run();
}

