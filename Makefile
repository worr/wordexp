CFLAGS := -std=c99 -Wall -Werror -Wextra -pedantic `pkg-config --cflags glib-2.0`
LDFLAGS := `pkg-config --libs glib-2.0`

.PHONY: clean check all

TESTS := 01_basic

all: $(TESTS)

01_basic: 01_basic.o
	$(CC) -o $@ $(LDFLAGS) $<

#02_basic: 02_basic.o
#	$(CC) -o $@ $(LDFLAGS) $<

check: $(TESTS)
	@-gtester --keep-going --verbose $^

clean:
	-rm *.o $(TESTS)
